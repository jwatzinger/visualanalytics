#pragma once
#include "Core\Dll.h"

namespace acl
{
	namespace core
	{
		class IModule;
	}

	namespace editor
	{
		class IPlugin;
	}
}

using namespace acl;

extern "C" __declspec(dllexport) core::IModule& CreateModule(void);
extern "C" __declspec(dllexport) editor::IPlugin& CreatePlugin(core::IModule& module);