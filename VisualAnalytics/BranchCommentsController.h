#pragma once
#include "BaseDataController.h"
#include "API\RenderPreview.h"

namespace acl
{
	namespace gui
	{
		class Texture;
	}

	namespace gfx
	{
		class Line;
	}

	namespace visualAnalytics
	{
		struct Data;

		class BranchCommentsController :
			public BaseDataController
		{
			using ClampedDataVector = std::vector<float>;
		public:
			BranchCommentsController(gui::Module& module, const gfx::ITextureLoader& textures, render::Renderer& renderer, const gfx::IZBufferLoader& zBuffer, const gfx::Screen& screen, const Data& data);
			~BranchCommentsController(void);

			void OnRender(gfx::Line& line) override;

		private:

			ClampedDataVector m_vComments, m_vBranches;
		};

	}
}


