#pragma once
#include "Checkpoint.h"
#include "Core\IModule.h"
#include "Gfx\EffectAsset.h"

namespace acl
{
	namespace gui
	{
		class DockArea;
	}

	namespace visualAnalytics
	{
		class FilesLinesController;
		class BranchCommentsController;

		class Module final :
			public core::IModule
		{
		public:

			Module(void);
			~Module(void);

			void OnInit(const core::GameStateContext& context) override;
			void OnUninit(const core::GameStateContext& context) override;
			void OnUpdate(double dt) override;
			void OnRender(double alpha) const override;

		private:

			gfx::EffectAssetPtr m_pEffect, m_pAreaEffect;
			sys::Pointer<gui::DockArea> m_pArea;

			sys::Pointer<Data> m_pData;
			sys::Pointer<FilesLinesController> m_pFilesLines;
			sys::Pointer<BranchCommentsController> m_pBranchComments;

			const core::GameStateContext* m_pCtx;
		};

	}
}

