#pragma once
#include <string>
#include <vector>
#include "System\ClassHelper.h"
#include "System\Pointer.h"

namespace acl
{
	namespace visualAnalytics
	{

		struct Checkpoint
		{
			std::string strName;
			size_t files, lines, statements;
			float branches, comments;
			size_t classdefs;
			float methodsPerClass, avgStatementsPerMethod;
			size_t maxComplexity, maxDepth;
			float avgDepth, avgComplexity;
			size_t functions;
		};

		struct Data
		{ 
			Data(void) = default;

			NON_COPYABLE(Data);

			using CheckpointVector = std::vector<sys::Pointer<Checkpoint>>;

			CheckpointVector vCheckpoints;
		};

	}
}