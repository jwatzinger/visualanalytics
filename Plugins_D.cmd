@echo off
IF "%1"=="" GOTO ALL
GOTO SPEC

:ALL
echo Copy Everything
for /f %%i in (PluginsList.txt) do (
xcopy /y "Debug\%%i_D.dll" "Game\Plugins\%%i\"
)
GOTO END

:SPEC
echo Copy %1%
pushd ..\
xcopy /y "Debug\%1_D.dll" "Game\Plugins\%1%\"
popd


:END
exit
