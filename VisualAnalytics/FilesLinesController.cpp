#include "FilesLinesController.h"
#include "Checkpoint.h"
#include "Gui\Texture.h"
#include "Gui\Area.h"
#include "Gui\Label.h"
#include "Gfx\Line.h"
#include "Render\Stage.h"
#include "System\Convert.h"

namespace acl
{
	namespace visualAnalytics
	{

		const int NUM_SEQUENCES = 8;
		const gfx::Color LINE_COLOR = gfx::Color(255, 120, 0);
		const gfx::Color FILE_COLOR = gfx::Color(0, 120, 255);

		FilesLinesController::FilesLinesController(gui::Module& module, const gfx::ITextureLoader& textures, render::Renderer& renderer, const gfx::Screen& screen, const Data& data) :
			BaseDataController(module, textures, renderer, nullptr, screen, data, L"Lines vs. Files")
		{
			auto& texture = GetTexture();

			// value labels
			auto& lineLabel = AddWidget<gui::Label>(0.0f, 0.0f, 128.0f, 32.0f, L"LINES");
			lineLabel.SetAlign(DT_LEFT | DT_VCENTER);
			lineLabel.SetTextSize(32);
			lineLabel.SetColor(&LINE_COLOR);
			lineLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
			lineLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			lineLabel.SetPadding(gui::PaddingSide::LEFT, 8);
			texture.AddChild(lineLabel);

			auto& fileLabel = AddWidget<gui::Label>(1.0f, 0.0f, 128.0f, 32.0f, L"FILES");
			fileLabel.SetAlign(DT_RIGHT | DT_VCENTER);
			fileLabel.SetTextSize(32);
			fileLabel.SetColor(&FILE_COLOR);
			fileLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
			fileLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			fileLabel.SetPadding(gui::PaddingSide::RIGHT, 8);
			fileLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			texture.AddChild(fileLabel);

			// calculate max values
			int maxLines = 0, maxFiles = 0;
			for(const Checkpoint* pValue : data.vCheckpoints)
			{
				maxLines = max(maxLines, pValue->lines);
				maxFiles = max(maxFiles, pValue->files);
			}

			// clamp values to range
			const auto numCheckpoints = data.vCheckpoints.size();
			m_vLines.reserve(numCheckpoints);
			m_vFiles.reserve(numCheckpoints);

			for(const Checkpoint* pValue : data.vCheckpoints)
			{
				m_vLines.emplace_back(pValue->lines / (float)maxLines);
				m_vFiles.emplace_back(pValue->files / (float)maxFiles);
			}

			// create value labels
			auto& area = GetMainArea();
			for(int i = 0; i <= NUM_SEQUENCES; i++)
			{
				const auto y = i / (float)NUM_SEQUENCES;

				const auto strLine = conv::ToString((int)(y * maxLines));
				const auto strFile = conv::ToString((int)(y * maxFiles));

				auto& lineLabel = AddWidget<gui::Label>(0.0f, 1.0f - y, 128.0f, 32.0f, strLine);
				lineLabel.SetAlign(DT_CENTER | DT_VCENTER);
				lineLabel.SetTextSize(24);
				lineLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
				lineLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
				lineLabel.SetCenter(gui::HorizontalAlign::LEFT, gui::VerticalAlign::CENTER);

				auto& fileLabel = AddWidget<gui::Label>(1.0f, 1.0f - y, 128.0f, 32.0f, strFile);
				fileLabel.SetAlign(DT_CENTER | DT_VCENTER);
				fileLabel.SetTextSize(24);
				fileLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
				fileLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
				fileLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::CENTER);

				if(i == NUM_SEQUENCES)
				{
					lineLabel.SetPadding(gui::PaddingSide::TOP, 16);
					fileLabel.SetPadding(gui::PaddingSide::TOP, 16);
				}

				area.AddChild(lineLabel);
				area.AddChild(fileLabel);
			}

			// add hover areas
			const auto step = 1.0f / data.vCheckpoints.size();
			int i = 0;
			for(const Checkpoint* pValue : data.vCheckpoints)
			{
				const auto x = step * i;
				auto& area = AddWidget<gui::Area>(x, 0.0f, step, 1.0f);
				area.SetCenter(gui::HorizontalAlign::CENTER, gui::VerticalAlign::TOP);
				area.AddDataContainer(area.SigReleased, (size_t)i).SigAccess.Connect(this, &FilesLinesController::OnSelectOwnCheckpoint);

				std::wstring stTooltip;
				stTooltip += L"Checkpoint name: " + conv::ToW(pValue->strName) + L'\n';
				stTooltip += L"Files:" + conv::ToString(pValue->files) + L'\n';
				stTooltip += L"Lines:" + conv::ToString(pValue->lines);

				area.SetTooltip(stTooltip);
				GetTexture().AddChild(area);

				i++;
			}
		}

		FilesLinesController::~FilesLinesController(void)
		{
		}

		void FilesLinesController::OnRender(gfx::Line& line)
		{
			// draw lines
			DrawData(m_vLines, line, LINE_COLOR);
			DrawData(m_vFiles, line, FILE_COLOR);
		}

	}
}