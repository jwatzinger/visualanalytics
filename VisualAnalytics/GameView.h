#pragma once
#include "API\IGameView.h"

namespace acl
{
	namespace visualAnalytics
	{

		class GameView :
			public editor::IGameView
		{
		public:
			GameView(void);
			~GameView(void);

			void OnInit(const gfx::Context & gfx, const render::Renderer & renderer) override;
			void OnSelectEntity(const ecs::EntityHandle & entity) override;
			void OnGotoEntity(const ecs::Entity & entity) override;
			ecs::EntityHandle OnPickEntity(const ecs::EntityManager & entities, const math::Vector2 & vMouse) override;
			void OnDropCreatePrefab(ecs::Entity & entity, const math::Vector2 & vPos) const override;
			void OnActiveEntityGizmo(bool active) override;
			void OnContextMenu(gui::ContextMenu & menu) override;
			void OnPastedEntity(ecs::Entity & entity) override;
			void OnKeyPress(gui::Keys key) override;
			void OnUpdate(void) override;
			void OnRender(double alpha) const override;
		};

	}
}


