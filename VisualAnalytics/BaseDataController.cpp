#include "BaseDataController.h"
#include "Checkpoint.h"
#include "Gui\Texture.h"
#include "Gui\Area.h"
#include "Gui\Label.h"
#include "Gui\Image.h"
#include "Gfx\Line.h"
#include "Gfx\SpriteBatch.h"
#include "Gfx\Screen.h"
#include "Render\Stage.h"
#include "System\Convert.h"

namespace acl
{
	namespace visualAnalytics
	{

		const int NUM_SEQUENCES = 8;

		BaseDataController::BaseDataController(gui::Module& module, const gfx::ITextureLoader& textures, render::Renderer& renderer, const gfx::IZBufferLoader* pZBuffer, const gfx::Screen& screen, const Data& data, const std::wstring& stLabel) :
			BaseDockController(module, stLabel), m_render(textures, renderer, math::Vector2(1, 1), stLabel, L"PreGui", pZBuffer), m_selectedCheckpoint(NO_CHECKPOINT), m_numDataItems(data.vCheckpoints.size())
		{
			m_pArea = &AddWidget<gui::Area>(0.0f, 0.0f, 1.0f, 1.0f);
			m_pArea->SetPadding(0, 0, 0, 128);
			m_pArea->SetClipping(false);
			
			m_pTexture = &AddWidget<gui::Texture>(0.0f, 0.0f, 1.0f, 1.0f, nullptr);
			m_pTexture->SetPadding(128, 0, 256, 0);
			m_pTexture->SetClipping(true);
			m_pArea->AddChild(*m_pTexture);

			// checkpoint labels
			auto& bottomArea = AddWidget<gui::Area>(0.0f, 1.0f, 1.0f, 128.0f);
			bottomArea.SetPadding(128, 0, 256, 0);
			bottomArea.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			bottomArea.SetCenter(gui::HorizontalAlign::LEFT, gui::VerticalAlign::BOTTOM);
			for(int i = 0; i < 6; i++)
			{
				const float x = i / 5.0f;

				const auto strCheckPoint = conv::ToW(data.vCheckpoints[(size_t)(x * (data.vCheckpoints.size()-1))]->strName);

				const auto width = m_pModule->CalculateTextRect(strCheckPoint, 16.0f).width;
				auto& checkpointLabel = AddWidget<gui::Label>(x, 0.0f, (float)width, 32.0f, strCheckPoint);
				checkpointLabel.SetAlign(DT_CENTER | DT_VCENTER);
				checkpointLabel.SetTextSize(16);
				checkpointLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
				checkpointLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);

				gui::HorizontalAlign hCenter;
				if(i == 0)
					hCenter = gui::HorizontalAlign::LEFT;
				else if(i == 5)
					hCenter = gui::HorizontalAlign::RIGHT;
				else
					hCenter = gui::HorizontalAlign::CENTER;

				checkpointLabel.SetCenter(hCenter, gui::VerticalAlign::TOP);

				bottomArea.AddChild(checkpointLabel);
			}

			// setup stage
			auto& stage = m_render.GetStage();

			const auto vScreenSize = screen.GetSize();
			const float constants[4] = { (float)vScreenSize.x, (float)vScreenSize.y, 0.0f, 0.0f };
			stage.SetShaderConstant(0, constants, 1);
			stage.SetClearColor(gfx::FColor(1.0f, 1.0f, 1.0f, 1.0f));
		}

		BaseDataController::~BaseDataController(void)
		{
		}

		void BaseDataController::Update(void)
		{
			BaseDockController::Update();

			const math::Vector2 vSize(m_pTexture->GetWidth(), m_pTexture->GetHeight());

			if(m_render.GetSize() != vSize)
			{
				m_render.Resize(vSize);

				m_pTexture->SetTexture(&m_render.GetRenderTarget());
			}
		}

		void BaseDataController::Render(gfx::Line& line, gfx::SpriteBatch& sprite, gfx::Effect& effect, gfx::Effect& specialEffect)
		{
			auto& stage = m_render.GetStage();

			const auto width = m_pTexture->GetWidth();
			const auto height = m_pTexture->GetHeight();

			// draw background lines
			line.SetColor(gfx::Color(255, 255, 255, 125));
			{
				const auto step = height / NUM_SEQUENCES;
				for(int i = 1; i <= NUM_SEQUENCES; i++)
				{
					const math::Vector3 vFrom(0.0f, step * i, 0.7f);
					const math::Vector3 vTo = vFrom + math::Vector3(width, 0.0f, 0.0f);
					line.SetLine(vFrom, vTo);
					line.Draw(stage, 1.0f);
				}
			}

			line.SetEffect(specialEffect);
			OnRender(line);
			line.SetEffect(effect);

			// render selection

			if(m_selectedCheckpoint != NO_CHECKPOINT)
			{
				const auto segmentWidth = (int)(width / m_numDataItems);
				const auto x = segmentWidth * m_selectedCheckpoint - segmentWidth / 2; // offset x by half segment width for better classification
				sprite.Begin(m_render.GetStage(), gfx::Sorting::NONE);

				sprite.SetSrcRect(math::Rect(124, 16, 8, 8));
				sprite.SetZ(0.7f);
				sprite.SetPosition(x, 0);
				sprite.SetSize(segmentWidth, height);
				sprite.SetClipRect(nullptr);
				sprite.Draw(gfx::Color(255, 255, 255, 125));

				sprite.End();
			}

			m_pTexture->MarkDirtyRect();
		}

		void BaseDataController::OnSelectCheckpoint(size_t checkpoint)
		{
			if(m_selectedCheckpoint == checkpoint)
				m_selectedCheckpoint = NO_CHECKPOINT;
			else
				m_selectedCheckpoint = checkpoint;
		}

		gui::Texture & BaseDataController::GetTexture(void)
		{
			return *m_pTexture;
		}

		gui::Area & BaseDataController::GetMainArea(void)
		{
			return *m_pArea;
		}

		void BaseDataController::DrawData(const ClampedDataVector& vData, gfx::Line& line, gfx::Color color, float z)
		{
			const auto height = m_pTexture->GetHeight();
			const auto step = m_pTexture->GetWidth() / vData.size();

			gfx::PointVector vPoints;

			line.SetColor(color);

			// draw lines
			unsigned int x = 0;
			for(const auto data : vData)
			{
				const auto y = height - (data * height);
				vPoints.emplace_back((float)x, y, z);

				x += step;
			}

			line.DrawMulti(vPoints, m_render.GetStage(), 2.0f);
		}

		void BaseDataController::OnSelectOwnCheckpoint(size_t checkpoint)
		{
			OnSelectCheckpoint(checkpoint);

			SigCheckpointSelected(checkpoint);
		}


	}
}