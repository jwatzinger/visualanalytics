#include "EditorPlugin.h"
#include "Module.h"
#include "GameView.h"
#include "API\IEditor.h"
#include "API\GameView.h"

namespace acl
{
	namespace visualAnalytics
	{

		EditorPlugin::EditorPlugin(core::IModule& module)
		{
			m_pModule = (Module*)&module;
		}

		void EditorPlugin::OnInit(editor::IEditor& editor, const core::GameStateContext& ctx)
		{
			editor.SetGameView(new GameView);

			editor.GetGameView().AddTexture(L"Gui", L"VisualAnalytics\\Gui");
		}
		
		void EditorPlugin::OnUpdate(void)
		{
		}

		void EditorPlugin::OnRender(double alpha) const
		{
		}

		void EditorPlugin::OnUninit(void)
		{
		}

		void EditorPlugin::OnBeginTest(void)
		{
		}

		void EditorPlugin::OnEndTest(void)
		{
		}

	}
}

