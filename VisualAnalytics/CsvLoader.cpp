#include "CsvLoader.h"
#include <fstream>
#include "File\File.h"
#include "System\Assert.h"

namespace acl
{
	namespace visualAnalytics
	{

		sys::Pointer<Data> CsvLoader::LoadData(const std::wstring& stFile)
		{
			std::ifstream stream(file::fullPath(stFile));
			if(!stream.is_open())
				return nullptr;

			// skip all data item names, we know those already
			std::string strTemp;
			for(size_t i = 0; i < 15; i++)
			{
				stream >> strTemp;
			}

			ACL_ASSERT(strTemp == "Functions"); // last value needs to be "Functions", otherwise we made a mistake parsing

			auto pData = sys::makePointer<Data>();

			while(stream.good() && !stream.eof())
			{
				auto pCheckpoint = LoadCheckpoint(stream);

				if(!pCheckpoint)
					break;

				pData->vCheckpoints.emplace_back(std::move(pCheckpoint));
			}

			return pData;
		}

		sys::Pointer<Checkpoint> CsvLoader::LoadCheckpoint(std::istream& stream)
		{
			auto pCheckpoint = sys::makePointer<Checkpoint>();

			stream >> pCheckpoint->strName;
			if(pCheckpoint->strName.empty())
				return nullptr;

			// we don't require date for this visualisation, so we just skip it
			std::string strDate;
			stream >> strDate;

			stream >> pCheckpoint->files;
			stream >> pCheckpoint->lines;
			stream >> pCheckpoint->statements;
			stream >> pCheckpoint->branches;
			stream >> pCheckpoint->comments;
			stream >> pCheckpoint->classdefs;
			stream >> pCheckpoint->methodsPerClass;
			stream >> pCheckpoint->avgStatementsPerMethod;
			stream >> pCheckpoint->maxComplexity;
			stream >> pCheckpoint->maxDepth;
			stream >> pCheckpoint->avgDepth;
			stream >> pCheckpoint->avgComplexity;
			stream >> pCheckpoint->functions;

			return pCheckpoint;
		}

	}
}


