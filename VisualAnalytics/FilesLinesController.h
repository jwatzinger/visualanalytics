#pragma once
#include "BaseDataController.h"
#include "API\RenderPreview.h"

namespace acl
{
	namespace gui
	{
		class Texture;
	}

	namespace gfx
	{
		class Line;
	}

	namespace visualAnalytics
	{
		struct Data;

		class FilesLinesController :
			public BaseDataController
		{
		public:
			FilesLinesController(gui::Module& module, const gfx::ITextureLoader& textures, render::Renderer& renderer, const gfx::Screen& screen, const Data& data);
			~FilesLinesController(void);

			void OnRender(gfx::Line& line) override;

		private:

			ClampedDataVector m_vLines, m_vFiles;
		};

	}
}


