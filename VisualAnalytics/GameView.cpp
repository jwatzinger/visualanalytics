#include "GameView.h"

namespace acl
{
	namespace visualAnalytics
	{

		GameView::GameView(void)
		{
		}

		GameView::~GameView(void)
		{
		}

		void GameView::OnInit(const gfx::Context & gfx, const render::Renderer & renderer)
		{
		}

		void GameView::OnSelectEntity(const ecs::EntityHandle & entity)
		{
		}

		void GameView::OnGotoEntity(const ecs::Entity & entity)
		{
		}

		ecs::EntityHandle GameView::OnPickEntity(const ecs::EntityManager & entities, const math::Vector2 & vMouse)
		{
			return ecs::EntityHandle();
		}

		void GameView::OnDropCreatePrefab(ecs::Entity & entity, const math::Vector2 & vPos) const
		{
		}

		void GameView::OnActiveEntityGizmo(bool active)
		{
		}

		void GameView::OnContextMenu(gui::ContextMenu & menu)
		{
		}

		void GameView::OnPastedEntity(ecs::Entity & entity)
		{
		}

		void GameView::OnKeyPress(gui::Keys key)
		{
		}

		void GameView::OnUpdate(void)
		{
		}

		void GameView::OnRender(double alpha) const
		{
		}

	}
}

