#pragma once
#include "API\BaseDockController.h"
#include "API\RenderPreview.h"

namespace acl
{
	namespace gui
	{
		class Texture;
		class Area;
	}

	namespace gfx
	{
		class Line;
		class Effect;
		class SpriteBatch;
		class Screen;
	}

	namespace visualAnalytics
	{
		struct Data;

		class BaseDataController :
			public editor::BaseDockController
		{
			const size_t NO_CHECKPOINT = -1;
		protected:
			using ClampedDataVector = std::vector<float>;
		public:
			BaseDataController(gui::Module& module, const gfx::ITextureLoader& textures, render::Renderer& renderer, const gfx::IZBufferLoader* pZBuffer, const gfx::Screen& screen, const Data& data, const std::wstring& stLabel);
			~BaseDataController(void);

			void Update(void) override final;
			void Render(gfx::Line& line, gfx::SpriteBatch& sprite, gfx::Effect& effect, gfx::Effect& specialEffect);

			void OnSelectCheckpoint(size_t checkpoint);

			core::Signal<size_t> SigCheckpointSelected;

		protected:

			gui::Texture& GetTexture(void);
			gui::Area& GetMainArea(void);

			void DrawData(const ClampedDataVector & vData, gfx::Line& line, gfx::Color color, float z = 0.5f);

			void OnSelectOwnCheckpoint(size_t checkpoint);

		private:

			virtual void OnRender(gfx::Line& line) = 0;

			editor::RenderPreview m_render;

			gui::Texture* m_pTexture;
			gui::Area* m_pArea;

			size_t m_selectedCheckpoint, m_numDataItems;
		};

	}
}


