#pragma once
#include "Checkpoint.h"
#include "System\Pointer.h"

namespace acl
{
	namespace visualAnalytics
	{

		class CsvLoader
		{
		public:
			
			static sys::Pointer<Data> LoadData(const std::wstring& stFile);

			static sys::Pointer<Checkpoint> LoadCheckpoint(std::istream& stream);
		};

	}
}


