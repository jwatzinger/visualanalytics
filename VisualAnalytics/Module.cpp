#include "Module.h"
#include "CsvLoader.h"
#include "FilesLinesController.h"
#include "BranchCommentsController.h"
#include "Asset\Storage.h"
#include "Core\BaseContext.h"
#include "Gui\DockArea.h"
#include "Gfx\Line.h"
#include "Gfx\FullscreenEffect.h"
#include "Render\Renderer.h"
#include "Render\Stage.h"

namespace acl
{
	namespace visualAnalytics
	{

		USE_ASSET_OBJECT(gfx::Texture)
		USE_ASSET_OBJECT(gfx::Effect)
		USE_ASSET_OBJECT(gfx::IMesh)

		Module::Module(void) : m_pCtx(nullptr)
		{
		}

		Module::~Module(void)
		{
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pCtx = &context;

			auto pGuiStage = context.render.renderer.GetStage(L"gui");

			// scene stage
			auto pScene = context.asset.storage.GetAssetDataByName<gfx::Texture>(L"VisualAnalytics\\Gui");
			pGuiStage->SetRenderTarget(0, pScene);

			// line effect
			m_pEffect = m_pCtx->asset.storage.GetAssetByName<gfx::Effect>(L"VisualAnalytics\\GuiLines");
			m_pAreaEffect = m_pCtx->asset.storage.GetAssetByName<gfx::Effect>(L"VisualAnalytics\\GuiAreaLines");
			m_pCtx->gfx.line.SetEffect(**m_pEffect);

			auto pMesh = m_pCtx->asset.storage.GetAssetDataByName<gfx::IMesh>(L"VisualAnalytics\\screenQuad");
			m_pCtx->gfx.effect.SetMesh(pMesh);

			// test load
			m_pData = CsvLoader::LoadData(L"Metrics.csv");

			m_pArea = sys::makePointer<gui::DockArea>(0.0f, 0.0f, 1.0f, 1.0f);
			m_pArea->SetDockState(gui::DockState::FIXED);
			m_pCtx->gui.module.GetMainWidget().AddChild(*m_pArea);
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
		}

		void Module::OnUpdate(double dt)
		{
			if(!m_pFilesLines)
			{
				m_pFilesLines = sys::makePointer<FilesLinesController>(m_pCtx->gui.module, m_pCtx->gfx.load.textures, m_pCtx->render.renderer, m_pCtx->gfx.screen, *m_pData);
				m_pBranchComments = sys::makePointer<BranchCommentsController>(m_pCtx->gui.module, m_pCtx->gfx.load.textures, m_pCtx->render.renderer, m_pCtx->gfx.load.zbuffers, m_pCtx->gfx.screen, *m_pData);

				m_pFilesLines->SigCheckpointSelected.Connect(m_pBranchComments.Get(), &BranchCommentsController::OnSelectCheckpoint);
				m_pBranchComments->SigCheckpointSelected.Connect(m_pFilesLines.Get(), &FilesLinesController::OnSelectCheckpoint);

				m_pArea->AddMiddle(m_pFilesLines->GetDock());
				m_pArea->AddDock(m_pBranchComments->GetDock(), gui::DockType::LEFT, 960);
			}
			m_pFilesLines->Update();
			m_pBranchComments->Update();
		}

		void Module::OnRender(double alpha) const
		{
			if(m_pFilesLines)
			{
				m_pFilesLines->Render(m_pCtx->gfx.line, m_pCtx->gfx.spriteBatch, **m_pEffect, **m_pEffect);
				m_pBranchComments->Render(m_pCtx->gfx.line, m_pCtx->gfx.spriteBatch, **m_pEffect, **m_pAreaEffect);
			}
		}

	}
}
