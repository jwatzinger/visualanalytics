#include "Export.h"
#include "Module.h"
#include "EditorPlugin.h"

core::IModule& CreateModule(void)
{
	return *new visualAnalytics::Module();
}

editor::IPlugin& CreatePlugin(core::IModule& module)
{
	return *new visualAnalytics::EditorPlugin(module);
}