#include "BranchCommentsController.h"
#include "Checkpoint.h"
#include "Gui\Texture.h"
#include "Gui\Area.h"
#include "Gui\Label.h"
#include "Gfx\Line.h"
#include "Render\Stage.h"
#include "System\Convert.h"

namespace acl
{
	namespace visualAnalytics
	{

		const int NUM_SEQUENCES = 8;
		const gfx::Color COMMENT_COLOR = gfx::Color(214, 40, 40);
		const gfx::Color BRANCH_COLOR = gfx::Color(44, 160, 44);

		BranchCommentsController::BranchCommentsController(gui::Module& module, const gfx::ITextureLoader& textures, render::Renderer& renderer, const gfx::IZBufferLoader& zBuffer, const gfx::Screen& screen, const Data& data) :
			BaseDataController(module, textures, renderer, &zBuffer, screen, data, L"Comments vs. Branches")
		{
			auto& texture = GetTexture();

			// value labels
			auto& commentLabel = AddWidget<gui::Label>(1.0f, 0.0f, 128.0f, 32.0f, L"% COMMENTS");
			commentLabel.SetAlign(DT_RIGHT | DT_VCENTER);
			commentLabel.SetTextSize(32);
			commentLabel.SetColor(&COMMENT_COLOR);
			commentLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
			commentLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			commentLabel.SetPadding(gui::PaddingSide::RIGHT, 8);
			commentLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			texture.AddChild(commentLabel);

			auto& branchLabel = AddWidget<gui::Label>(1.0f, 40.0f, 128.0f, 32.0f, L"% BRANCHES");
			branchLabel.SetAlign(DT_RIGHT | DT_VCENTER);
			branchLabel.SetTextSize(32);
			branchLabel.SetColor(&BRANCH_COLOR);
			branchLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
			branchLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			branchLabel.SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			branchLabel.SetPadding(gui::PaddingSide::RIGHT, 8);
			branchLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			texture.AddChild(branchLabel);

			// calculate max values
			float maxValue = 0.0f;
			for(const Checkpoint* pValue : data.vCheckpoints)
			{
				maxValue = max(maxValue, pValue->comments + pValue->branches);
			}

			// clamp values to range
			const auto numCheckpoints = data.vCheckpoints.size();
			m_vComments.reserve(numCheckpoints);
			m_vBranches.reserve(numCheckpoints);

			for(const Checkpoint* pValue : data.vCheckpoints)
			{
				const auto commentValue = pValue->comments / (float)maxValue;
				m_vComments.emplace_back(commentValue);
				m_vBranches.emplace_back(commentValue + (pValue->branches / (float)maxValue));
			}

			// create value labels
			auto& area = GetMainArea();
			for(int i = 0; i <= NUM_SEQUENCES; i++)
			{
				const auto y = i / (float)NUM_SEQUENCES;

				const auto strPercent = conv::ToString(y * maxValue);

				auto& commentLabel = AddWidget<gui::Label>(0.0f, 1.0f - y, 128.0f, 32.0f, strPercent);
				commentLabel.SetAlign(DT_CENTER | DT_VCENTER);
				commentLabel.SetTextSize(24);
				commentLabel.SetPositionMode(gui::Position::WIDTH, gui::PositionMode::ABS);
				commentLabel.SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
				commentLabel.SetCenter(gui::HorizontalAlign::LEFT, gui::VerticalAlign::CENTER);

				if(i == NUM_SEQUENCES)
					commentLabel.SetPadding(gui::PaddingSide::TOP, 16);

				area.AddChild(commentLabel);
			}

			// add hover areas
			const auto step = 1.0f / data.vCheckpoints.size();
			int i = 0;
			for(const Checkpoint* pValue : data.vCheckpoints)
			{
				const auto x = step * i;
				auto& area = AddWidget<gui::Area>(x, 0.0f, step, 1.0f);
				area.SetCenter(gui::HorizontalAlign::CENTER, gui::VerticalAlign::TOP);
				area.AddDataContainer(area.SigReleased, (size_t)i).SigAccess.Connect(this, &BranchCommentsController::OnSelectOwnCheckpoint);

				std::wstring stTooltip;
				stTooltip += L"Checkpoint name: " + conv::ToW(pValue->strName) + L'\n';
				stTooltip += L"Comments:" + conv::ToString(pValue->comments) + L'\n';
				stTooltip += L"Branches:" + conv::ToString(pValue->branches);

				area.SetTooltip(stTooltip);
				GetTexture().AddChild(area);

				i++;
			}
		}

		BranchCommentsController::~BranchCommentsController(void)
		{
		}

		void BranchCommentsController::OnRender(gfx::Line& line)
		{
			// draw lines
			DrawData(m_vComments, line, COMMENT_COLOR, 0.6f);
			DrawData(m_vBranches, line, BRANCH_COLOR);
		}


	}
}