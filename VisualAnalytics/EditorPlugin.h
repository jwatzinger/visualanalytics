#pragma once
#include "API\IPlugin.h"

namespace acl
{
	namespace visualAnalytics
	{

		class Module;

		class EditorPlugin :
			public editor::IPlugin
		{
		public:

			EditorPlugin(core::IModule& module);

			void OnInit(editor::IEditor& editor, const core::GameStateContext& ctx) override;
			void OnUpdate(void) override;
			void OnRender(double alpha) const override;
			void OnUninit(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			Module* m_pModule;
		};

	}
}


